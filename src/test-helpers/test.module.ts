// import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
// import { FormsModule } from '@angular/forms';
// import { AppRoutingModule } from '../app/app-routing.module';
// import { Pipe, PipeTransform } from '@angular/core';
// import { AppComponent } from '../app/app.component';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { HttpModule } from '@angular/http';
// import { LogsService } from '../app/services/logs.service';
// import { ReactiveFormsModule } from '@angular/forms';
// import { WeekPipe } from '../app/pipes/week.pipe';
// import { NavbarComponent } from '../app/components/navbar/navbar.component';


// @NgModule({
//   declarations: [
//     AppComponent,
//     WeekPipe,
//     NavbarComponent
//   ],
//   imports: [
//     BrowserModule,
//     AppRoutingModule,
//     FormsModule,
//     HttpModule,
//     ReactiveFormsModule,
//     BrowserAnimationsModule
//   ],
//   providers: [LogsService],
//   bootstrap: [AppComponent],
//   exports: [BrowserModule,
//     WeekPipe,
//     AppRoutingModule,
//     FormsModule,
//     HttpModule,
//     ReactiveFormsModule,
//     BrowserAnimationsModule]
// })
// export class TestModule { }
